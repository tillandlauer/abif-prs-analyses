#! /bin/bash
gemma -bfile genotype_data -gk 1 -o genotype_data_GRM_centered
gemma -g data_PRS.bimbam -p data_pheno_a.bimbam -n 1 -c data_covars.bimbam -k output/genotype_data_GRM_centered.cXX.txt -km 1 -notsnp -r2 1 -lmm 4 -o results_analysis_a

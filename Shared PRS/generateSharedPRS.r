# Load the clumped GWAS summary statistics
bip <- readRDS("BD_GWAS_results_clumped.RDS")
scz <- readRDS("SCZ_GWAS_results_clumped.RDS")
mdd <- readRDS("MDD_GWAS_results_clumped.RDS")

# Select and align the relevant columns
bip <- bip[,c(1:5,9:11,18,20:23)]
colnames(bip) <- sub(".x","_BIP",colnames(bip))

scz <- scz[,c(1:5,7:9,11,13:16)]
colnames(scz) <- sub(".x","_SCZ",colnames(scz))
colnames(scz)[6:8] <- c("OR","SE","P")

mdd <- mdd[,c(1:5,9:11,12,14:17)]
colnames(mdd) <- sub(".x","_MDD",colnames(mdd))

# Merge the three files
temp1 <- merge(bip,scz,by=c("CHR","BP","SNP.y","A1.y","A2.y","minor_allele","major_allele"),suffixes=c("_BIP","_SCZ"))
all <- merge(temp1,mdd,by=c("CHR","BP","SNP.y","A1.y","A2.y","minor_allele","major_allele"),suffixes=c("","_MDD"))
nrow(all)
colnames(all)[23:25] <- paste0(colnames(all)[23:25],"_MDD")
colnames(all) <- sub(".y","_SPAIN",colnames(all)) 
saveRDS(all,"PRS_all_merged.RDS")

# For each disorder PRS, keep only nominally significant SNPs
scz <- all[which(all$P_SCZ<0.05 & all$P_BIP>=0.05 & all$P_MDD>=0.05),]
nrow(scz)
bip <- all[which(all$P_BIP<0.05 & all$P_SCZ>=0.05 & all$P_MDD>=0.05),]
nrow(bip)
mdd <- all[which(all$P_MDD<0.05 & all$P_SCZ>=0.05 & all$P_BIP>=0.05),]
nrow(mdd)
all_nominal <- all[which(all$P_SCZ<0.05 & all$P_BIP<0.05 & all$P_MDD<0.05),]
nrow(all_nominal)

# Check and align alleles
all_nominal$A1_BIP <- factor(all_nominal$A1_BIP)
levels(all_nominal$A1_BIP)
all_nominal$A2_BIP <- factor(all_nominal$A2_BIP)
levels(all_nominal$A2_BIP)

all_nominal$A1_SCZ <- factor(all_nominal$A1_SCZ)
levels(all_nominal$A1_SCZ)
all_nominal$A2_SCZ <- factor(all_nominal$A2_SCZ)
levels(all_nominal$A2_SCZ)

all_nominal$A1_MDD <- factor(all_nominal$A1_MDD)
levels(all_nominal$A1_MDD)
all_nominal$A2_MDD <- factor(all_nominal$A2_MDD)
levels(all_nominal$A2_MDD)

levels(all_nominal$A1_BIP) <- sub("^([[:alpha:]]*).*","\\1",levels(all_nominal$A1_BIP))
levels(all_nominal$A2_BIP) <- sub("^([[:alpha:]]*).*","\\1",levels(all_nominal$A2_BIP))
levels(all_nominal$A1_SCZ) <- sub("^([[:alpha:]]*).*","\\1",levels(all_nominal$A1_SCZ))
levels(all_nominal$A2_SCZ) <- sub("^([[:alpha:]]*).*","\\1",levels(all_nominal$A2_SCZ))

all_nominal$A1_BIP <- factor(all_nominal$A1_BIP)
all_nominal$A2_BIP <- factor(all_nominal$A2_BIP)
all_nominal$A1_SCZ <- factor(all_nominal$A1_SCZ)
all_nominal$A2_SCZ <- factor(all_nominal$A2_SCZ)

# Retain only SNPs that have effect sizes in the same direction across the three disorders
all_nominal <- all_nominal[which((all_nominal$OR_SCZ>=1 & all_nominal$OR_BIP>=1 & all_nominal$OR_MDD>=1) | (all_nominal$OR_SCZ<1 & all_nominal$OR_BIP<1 & all_nominal$OR_MDD<1)),]
nrow(all_nominal)

# Select columns in preparation of the meta-analysis
all_nominal <- all_nominal[,c(1:13,17:19,23:25)]
colnames(all_nominal)[8:10] <- sub("_BIP","_PGC",colnames(all_nominal)[8:10])
scz <- scz[,c(1:13,17:19,23:25)]
colnames(scz)[8:10] <- sub("_BIP","_PGC",colnames(scz)[8:10])
bip <- bip[,c(1:13,17:19,23:25)]
colnames(bip)[8:10] <- sub("_BIP","_PGC",colnames(bip)[8:10])
mdd <- mdd[,c(1:13,17:19,23:25)]
colnames(mdd)[8:10] <- sub("_BIP","_PGC",colnames(mdd)[8:10])

saveRDS(all_nominal,"PRS_all_merged_nominal.RDS")

# Meta-analysis
library(meta)

all_nominal$BETA_BIP <- log(all_nominal$OR_BIP)
all_nominal$BETA_SCZ <- log(all_nominal$OR_SCZ)
all_nominal$BETA_MDD <- log(all_nominal$OR_MDD)

# Random effects meta-analysis
for(j in seq(1,nrow(all_nominal)))
	{
	temp <- metagen(c(all_nominal[j,"BETA_BIP"],all_nominal[j,"BETA_SCZ"],all_nominal[j,"BETA_MDD"]),c(all_nominal[j,"SE_BIP"],all_nominal[j,"SE_SCZ"],all_nominal[j,"SE_MDD"]),c("BIP","SCZ","MDD"))
	meta_temp <- as.data.frame(t(c(as.character(all_nominal[j,"SNP_SPAIN"]),temp$TE.random,temp$seTE.random,exp(temp$TE.random),exp(temp$lower.random),exp(temp$upper.random),temp$pval.random)),stringsAsFactors=F)
	colnames(meta_temp) <- c("SNP","BETA","SE","OR","CI_l","CI_u","P")
	meta_temp[,2:ncol(meta_temp)] <- sapply(meta_temp[,2:ncol(meta_temp)], as.numeric)
	if(j==1) meta_all <- meta_temp else meta_all <- rbind(meta_all,meta_temp)
	}
rownames(meta_all) <- seq(1:nrow(meta_all))

# Merge the meta-analysis results with the summary statistics of selected SNPs of the single GWAS
all_nominal2 <- merge(meta_all,all_nominal,by.x="SNP",by.y="SNP_SPAIN")
rownames(all_nominal2) <- seq(1:nrow(all_nominal2))
saveRDS(all_nominal2,"PRS_all_merged_nominal_metaRandom.RDS")

# Use these new summary statistics for the calculation of PRS

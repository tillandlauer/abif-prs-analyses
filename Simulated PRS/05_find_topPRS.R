# Load data
pheno <- read.table("all_data.txt",h=T) # also contains PRS
steps <- as.numeric(sub("e\\.","e-",sub("PRS_BIP_","",colnames(pheno)[grep("PRS_BIP",colnames(pheno))]))) # get the PRS thresholds

endpoints <- c("FAMBD_CCcontrols")
endpoint <- endpoints[1]

# Load summary files
files <- dir("02_summary/")

# calculate the minimum mean p-value per endpoint
for (endpoint in endpoints)
  {
  files2 <- files[grep(paste0(endpoint),files)]
  for (file in files2)
    {
    temp <- read.table(paste0("02_summary/",file),h=T,stringsAsFactors=F)
    if(file==files2[1]) plist <- temp else plist <- rbind(plist,temp)
    }
  temp <- plist[which.min(plist$mean),] # calculate the minimum mean p-value per endpoint
  if(endpoint==endpoints[1]) result <- temp else result <- rbind(result,temp)
  }

result
write.table(result,paste0("PRS_simulated_testresults_minP.txt"),c=T,r=F,qu=F)


# Get the results for the four psychiatric PRS
files <- dir("01_results/",pattern="results",full.names=T)
for (endpoint in endpoints)
  {
  files2 <- files[grep(paste0(endpoint),files)] # get results of simulated PRS
  temp <- readRDS(files2[1])
  
  # Get the results for the four psychiatric PRS
  bip <- temp[which(temp$PRS=="BIP"),c("ANALYSIS","Threshold","P_1sided")]
  bip$Threshold <- steps
  scz <- temp[which(temp$PRS=="SCZ"),c("ANALYSIS","Threshold","P_1sided")]
  scz$Threshold <- steps
  mdd <- temp[which(temp$PRS=="MDD"),c("ANALYSIS","Threshold","P_1sided")]
  mdd$Threshold <- steps
  shared <- temp[which(temp$PRS=="Shared"),c("ANALYSIS","Threshold","P_1sided")]
  shared$Threshold <- steps
  
  if(endpoint==endpoints[1])
    {
    bip_res <- bip    
    scz_res <- scz    
    mdd_res <- mdd    
    shared_res <- shared    
    } else
    {
    bip_res <- rbind(bip_res,bip)    
    scz_res <- rbind(scz_res,scz)
    mdd_res <- rbind(mdd_res,mdd)
    shared_res <- rbind(shared_res,shared)    
    }
  }
    

# Compare simulated PRS to psychiatric PRS
for (endpoint in endpoints)
  {
  # Get the minimum p-value per psychiatric PRS
  temp <- bip_res[which(bip_res$ANALYSIS==endpoint),]
  bip <- temp[which.min(temp$P_1sided),]$P_1sided
  temp <- scz_res[which(scz_res$ANALYSIS==endpoint),]
  scz <- temp[which.min(temp$P_1sided),]$P_1sided
  temp <- mdd_res[which(mdd_res$ANALYSIS==endpoint),]
  mdd <- temp[which.min(temp$P_1sided),]$P_1sided
  temp <- shared_res[which(shared_res$ANALYSIS==endpoint),]
  shared <- temp[which.min(temp$P_1sided),]$P_1sided

  # Load the results of the simulated PRS
  thresh <- result[which(result$endpoint==endpoint),]$threshold
  results <- readRDS(paste0("01_results/PRS_GMMAT_simulated_",endpoint,"_",thresh,"_results.RDS"))
  results <- results[results$simPRS,]
  
  # Summarize results
  temp <- data.frame(cbind(thresh,endpoint,min(results$P_1sided),mean(results$P_1sided),sd(results$P_1sided),mean(results$OR),mean(results$CI_lower),mean(results$CI_upper),mean(results$BETA),sd(results$BETA)))
  colnames(temp) <- c("threshold","endpoint","P_min","P_mean","P_SD","OR_mean","CI_l_mean","CI_u_mean","BETA_mean","BETA_SD")
  temp[,3:ncol(temp)] <- sapply(temp[,3:ncol(temp)], function(x) as.numeric(as.character(x)))
  temp$P_SE <- temp$P_SD/sqrt(nrow(results))
  temp$P_CI_l <- temp$P_mean-1.959964*temp$P_SE
  temp$P_CI_u <- temp$P_mean+1.959964*temp$P_SE
  temp$BETA_SE <- temp$BETA_SD/sqrt(nrow(results))
  temp$BETA_CI_l <- temp$BETA_mean-1.959964*temp$BETA_SE
  temp$BETA_CI_u <- temp$BETA_mean+1.959964*temp$BETA_SE
  write.table(temp,paste0("PRS_simulated_summary_minP_",endpoint,".txt"),c=T,r=F,qu=F)
  

  # Calculate the number of simulated PRS with p <= p of disorder PRS
  (nBIP <- nrow(results[which(results$P_1sided<=bip),]))
  (nSCZ <- nrow(results[which(results$P_1sided<=scz),]))
  (nMDD <- nrow(results[which(results$P_1sided<=mdd),]))
  (nALL <- nrow(results[which(results$P_1sided<=allr),]))
  
  # Calculate corresponding p-values
  (pBIP <- nBIP/nrow(results))
  (pSCZ <- nSCZ/nrow(results))
  (pMDD <- nMDD/nrow(results))
  (pALL <- nALL/nrow(results))
  (1/nrow(results))
  
  # Summarize results
  final <- data.frame(cbind(c("BIP","SCZ","MDD","ALL"),c(nBIP,nSCZ,nMDD,nALL),c(c(pBIP,pSCZ,pMDD,pALL))))
  colnames(final) <- c("PRS","nSimulated","p")
  final[,2:3] <- sapply(final[,2:3],function(x) as.numeric(as.character(x)))
  if(any((final$p==0))) final[which(final$p==0),]$p  <- paste0("<",1/nrow(results))
  final["nPerm"] <- nrow(results)
  final$threshold_simulated <- thresh
  final$endpoint <- endpoint
  print(final)
  write.table(final,paste0("PRS_simulated_testResults_minP_",endpoint,".txt"),c=T,r=F,qu=F)
  }


# Add confidence intervals
library(binom)
library(writexl)

# Load results files
files <- dir(pattern="PRS_simulated_testresults_minP_")
files <- files[grep("txt",files)]
fn <- files[1]

for (fn in files)
  {
  temp <- read.table(fn,h=T)
  temp$prob <- NA
  temp$CI_lower <- NA
  temp$CI_upper <- NA
  
  # Calculate confidence intervals using the binomial test
  for (i in 1:nrow(temp))
    {
    temp2 <- binom.confint(temp$nRandom[i],temp$nPerm[i],method="exact")
    temp$prob[i] <- temp2$mean
    temp$CI_lower[i] <- temp2$lower
    temp$CI_upper[i] <- temp2$upper
    }
  
  temp$CI <- paste0(round(temp$CI_lower,dig=3),"-",round(temp$CI_upper,dig=3))
  temp <- temp[,c(6,1,5,2,7,10)]
  temp$threshold_simulated <- format(temp$threshold_simulated,sci=T)
  write_xlsx(temp,paste0(sub(".txt","",fn),".xlsx")) # save results
  }

#!/bin/sh
# run this script using SLURM: sbatch --array 1-100 --mem=16G -J simPRS --wrap="./02_simulatePRS.sh n" # where n is the number of an item in the list of PRS thresholds
Rscript 02b_simulatePRS.R ${1} ${SLURM_ARRAY_TASK_ID}
